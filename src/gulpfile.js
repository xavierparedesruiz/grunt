/*
* Dependencias
*/
var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  imagemin = require('gulp-tinypng'),
  watch = require('gulp-watch'),
  autoprefixer = require('gulp-autoprefixer');

/*
* Configuración de la tarea 'demo'
*/

gulp.task('minimage', function () {
  gulp.src('src/img/*')
  .pipe(imagemin('q9RoCififEyyoY9ryg0tMaG91gA_Zo-G'))
  .pipe(gulp.dest('build/img'))
});

gulp.task('minicss', function () {
  gulp.src('src/css/*.css')
  .pipe(autoprefixer({
    browsers:['last 2 versions'],
    cascade: false
   }))
  .pipe(concat('all.css'))
  .pipe(gulp.dest('build/css'))
});

gulp.task('automini',function() {
  return watch('src/css/*.css', 'minicss')
}); 



